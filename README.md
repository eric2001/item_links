# Item Links

## Description
Item Links is a module for Gallery 3 that allows users to convert existing gallery items (albums/photos/movies) into links to external URLs. Once this module is installed, all items will have a "Redirect to URL" field on the Edit album/Edit photo/Edit movie screens. Simply enter a URL (starting with "http://") into this field and press Modify to save. Afterwards any user trying to visit that items page will be automatically redirected to the specified URL.

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/102548).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "item_links" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.  Afterwards, edit any item in your gallery and set the "Redirect URL" field to the URL that the user should be sent to when the item is clicked on.

## History
**Version 1.1.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Released 06 November 2021.
>
> Download: [Version 1.1.0](/uploads/d4e20ccb8461cdd5a19cf9f25ce2ad2a/item_links110.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 13 June 2011.
>
> Download: [Version 1.0.0](/uploads/c314c1c88045214a5d58708e472a7955/item_links100.zip)
